
//3. Create a trainer object using object literals.
let trainer = {
	//4. Initialize/add the following trainer object properties:
	//  Name (String)
	// - Age (Number)
	// - Pokemon (Array)
	// - Friends (Object with Array values for properties)

	name: 'ash ketchum',
	age: 10,
	Pokemon: ['Ivysaur','Venusaur', 'Charmeleon'],
	friends:{'hoenn': ["May","Max"], 'kanto': ["Brock","Misty"]},

	//5. Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!
	talk: function(target) {
		console.log("Pikachu! I choose you!")
	}

}

//6. Access the trainer object properties using dot and square bracket notation.
console.log("Trainer's name is ", trainer['name']);
console.log("Trainer's age is ", trainer.age);
console.log("Trainer's pokemon is ", trainer['Pokemon']);
console.log("Trainer's friends is ", trainer.friends);
//7. Invoke/call the trainer talk object method.
trainer.talk();

// 8. Create a constructor for creating a pokemon with the following properties:
function myPokemon(name, level) {
	//Properties
	this.name = name;
	this.level = level;
	this.health = 2* level;
	this.attack = level;

	//10. Create a tackle method that will subtract the health property of the target pokemon object with the attack property of the object that used the tackle method.
	this.tackle = function(target){
		console.log(this.name+' tackled '+target['name']);
		let targetPokemonHealth = target.health - this.attack ;
		console.log(target.name + "'s health is now reduced to " +  targetPokemonHealth);
		//12. Create a condition in the tackle method that if the health property of the target pokemon object is less than or equal to 0 will invoke the faint method.
		if ( targetPokemonHealth <= 0){
			this.faint(target);
		} 
	}

	//11. Create a faint method that will print out a message of targetPokemon has fainted.
	this.faint = function(target){
		console.log(target.name + ' has fainted');
	}
}

//9. Create/instantiate several pokemon object from the constructor with varying name and level properties.
let rattata = new myPokemon("Rattata", 8);
let mankey =  new myPokemon("Mankey",12);
let persian =  new myPokemon("Persian",25);

//13. Invoke the tackle method of one pokemon object to see if it works as intended.
rattata.tackle(mankey);

persian.tackle(rattata);


















